package util;

import javax.persistence.Persistence;

import model.Animal;
import model.Diagnostic;
import model.PersonalMedical;
import model.Programari;

import java.sql.Time;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * @author Ana Clasa DatabaseUtil face posibila conexiunea la Baza de Date si
 *         permite efectuarea operatiilor CRUD asupra datelor din tabel.
 * @param EntityManagerFactory - se ocupa de Conexiune
 * @param EntityManager        - se ocupa de Tranzactii
 */
public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	private static DatabaseUtil dbUtil;

	private DatabaseUtil() {

	}

	public static DatabaseUtil getInstance() {
		if (dbUtil == null) {
			dbUtil = new DatabaseUtil();
		}
		return dbUtil;
	}

	/**
	 * @throws Exception - Arunca Exceptii in cazul in care nu se poate realiza
	 *                   conexiunea la Baza de Date Initializeaza cei 2
	 *                   parametri, @param EntityManagerFactory si @param
	 *                   EntityManager
	 */
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("CabinetVeterinarJPA");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	/**
	 * Commit
	 */
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	/**
	 * Functia opreste conexiunea cu Baza de Date
	 */
	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * @param animal
	 * @throws Exception - Arunca exceptii in cazul in care Animalul nu poate fi
	 *                   creat din motive precum inserarea unui ID deja existent,
	 *                   fapt imposibil din cauza ca acesta este o CHEIE PRIMARA
	 *                   UNICA Functia creeaza un nou Animal in tabelul din Baza de
	 *                   Date
	 */
	public <E> void createEntity(E entity) {
		try {
			entityManager.persist(entity);
			entityManager.getTransaction().commit();
			System.out.println("Entitatea a fost creata! Verificati Baza de Date!");
		} catch (Exception e) {
			System.out.println("EROARE! Exista deja o Entitate cu acest ID!");
			return;
		}
	}
	
	// ANIMAL
	/**
	 * @param id
	 * @return Returneaza Animalul cu ID - ul primit prin intermediul parametrului
	 *         id in cazul existentei acestuia NULL altfel
	 */
	public Animal findAnimal(int id) {
		Animal animal = entityManager.find(Animal.class, id);
		return animal;
	}

	/**
	 * @param id Afiseaza in consola informatiile aferente Animalului cu ID - ul
	 *           primit prin intermediul parametrului id
	 */
	public void readAnimal(int id) {
		Animal animal = findAnimal(id);
		if (animal == null)
			System.out.println("Animalul nu a fost gasit!");
		else
			System.out.println("Animalul : " + animal.getNume() + " are ID - ul : " + animal.getIdAnimal());
	}

	/**
	 * Functie de printare rezultate din baza de date - Afiseaza in consola toate
	 * Animalele din Baza de Date
	 */
	@SuppressWarnings("unchecked")
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from veterinar.animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal : " + animal.getNume() + " has ID : " + animal.getIdAnimal());
		}
	}

	/**
	 * @param id Functia ofera posibilitatea modificarii campului Nume al Animalului
	 *           cu ID - ul egal cu valoarea parametrului primit id din tabelul
	 *           Animal al Bazei de Date
	 */
	public void updateAnimal(int id) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduceti un nou Nume: ");
		String newName = input.nextLine();

		Animal animal = findAnimal(id);
		if (animal == null) {
			System.out.println("Animalul nu a fost gasit!");
		} else {
			animal.setNume(newName);
			entityManager.getTransaction().commit();
			System.out.println("Modificarea a fost efectuata! Verificati Baza de Date!");
		}
		input.close();
	}

	public List<Animal> animalList() {
		List<Animal> animalList = entityManager.createNativeQuery("Select * from veterinar.animal", Animal.class)
				.getResultList();
		return animalList;
	}

	public List<Diagnostic> diagnosticList() {
		List<Diagnostic> diagnosticList = entityManager
				.createNativeQuery("Select * from veterinar.diagnostic", Diagnostic.class).getResultList();
		return diagnosticList;
	}

	/**
	 * @param animal Functia ofera posibilitatea stergerii Animalului cu ID - ul
	 *               egal cu valoarea parametrului primit id din tabelul Animal al
	 *               Bazei de Date, cat si al programarilor aferente acestuia
	 */
	public void deleteAnimal(Animal animal) {
		List programari = new LinkedList<Programari>();
		programari = animal.getProgramaris();
		for (int index = 0; index < programari.size(); index++)
			entityManager.remove(programari.get(index));

		entityManager.remove(animal);
		entityManager.getTransaction().commit();
		System.out.println("Animalul si programarile aferente au fost sterse! Verificati Baza de Date!");
	}

	// PERSONAL MEDICAL

	/**
	 * @param id
	 * @return ReturneazaPersonalul Medical cu ID - ul primit prin intermediul
	 *         parametrului id in cazul existentei acestuia NULL altfel
	 */
	public PersonalMedical findPersonalMedical(int id) {
		PersonalMedical personalMedical = entityManager.find(PersonalMedical.class, id);
		return personalMedical;
	}

	public PersonalMedical findPersonalMedical(String nume) {
		PersonalMedical personalMedical = entityManager.find(PersonalMedical.class, nume);
		return personalMedical;
	}

	/**
	 * @param id Afiseaza in consola informatiile aferente Personalului Medical cu
	 *           ID - ul primit prin intermediul parametrului id
	 */
	public void readPersonalMedical(int id) {
		PersonalMedical personalMedical = findPersonalMedical(id);
		if (personalMedical == null)
			System.out.println("Personalul Medical nu a fost gasit! Verificati Baza de Date!");
		else
			System.out.println("Personalul Medical : " + personalMedical.getNume() + " " + personalMedical.getPrenume()
					+ " are ID - ul : " + personalMedical.getIdPersonalMedical());
	}

	/**
	 * @param id Functia ofera posibilitatea modificarii campului Nume al
	 *           Personalului Medical cu ID - ul egal cu valoarea parametrului
	 *           primit id din tabelul personal_medical al Bazei de Date
	 */
	public void updatePersonalMedical(int id) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduceti un nou Nume: ");
		String numeNou = input.nextLine();

		PersonalMedical personalMedical = findPersonalMedical(id);
		if (personalMedical == null) {
			System.out.println("Personalul Medical nu a fost gasit!");
		} else {
			personalMedical.setNume(numeNou);
			entityManager.getTransaction().commit();
			System.out.println("Modificarea a fost efectuata! Verificati Baza de Date!");
		}
		input.close();
	}

	public List<Programari> listaProgramari() {
		List<Programari> oreProgramari = entityManager
				.createNativeQuery("Select * from veterinar.programari", Programari.class).getResultList();
		return oreProgramari;
	}

	public List<Programari> listaProgramariPersMedical(int id) {
		List<Programari> oreProgramari = entityManager
				.createNativeQuery("Select * from veterinar.programari WHERE idPersonalMedical=id", Programari.class)
				.getResultList();
		return oreProgramari;
	}
	public List<Programari> listaProgramariPersMedicalDupaData(int id,String data) {
		List<Programari> programari = entityManager
				.createNativeQuery("Select * from veterinar.programari WHERE idPersonalMedical=id", Programari.class)
				.getResultList();
		for (Programari p:programari)
		{
			String currentDate=p.getStringData();
		}
		return programari;
	}

	/**
	 * @param personalMedical Functia ofera posibilitatea stergerii Personalului
	 *                        Medical cu ID - ul egal cu valoarea parametrului
	 *                        primit id din tabelul personal_medical al Bazei de
	 *                        Date, cat si al programarilor aferente acestuia
	 */
	public void deletePersonalMedical(PersonalMedical personalMedical) {
		List programari = new LinkedList<Programari>();
		programari = personalMedical.getProgramaris();
		for (int index = 0; index < programari.size(); index++)
			entityManager.remove(programari.get(index));

		entityManager.remove(personalMedical);
		entityManager.getTransaction().commit();
		System.out.println("Personalul Medical si programarile aferente au fost sterse! Verificati Baza de Date!");
	}

	/**
	 * Functie de printare rezultate din baza de date - Afiseaza in consola intregul
	 * Personal Medical din Baza de Date
	 */
	@SuppressWarnings("unchecked")
	public void printAllPersonalMedicalFromDB() {
		List<PersonalMedical> results = entityManager
				.createNativeQuery("Select * from veterinar.personal_medical", PersonalMedical.class).getResultList();

		for (PersonalMedical personalMedical : results) {
			System.out.println("Personalul Medical : " + personalMedical.getNume() + " " + personalMedical.getPrenume()
					+ " " + " are ID- ul : " + personalMedical.getIdPersonalMedical() + ".");
		}
	}

	// PROGRAMARI

	/**
	 * @param id
	 * @return - Returneaza Programarea cu ID - ul primit prin intermediul
	 *         parametrului id in cazul existentei acestuia NULL altfel
	 */
	public Programari findProgramare(int id) {
		Programari programare = entityManager.find(Programari.class, id);
		return programare;
	}

	/**
	 * @param id Afiseaza in consola informatiile aferente Programarii cu ID - ul
	 *           primit prin intermediul parametrului id La afisarea lunii, valoarea
	 *           acesteia este crescuta cu o unitate, default sunt afisate luni cu
	 *           valorile intre 0 si 11, luna Ianuarie reprezentand cifra 0 La
	 *           afisarea anului, valoarea acestuia este crescuta cu 1900, intrucat
	 *           default anul este memorat cu valoarea citita - 1900
	 */
	@SuppressWarnings("deprecation")
	public void readProgramare(int id) {
		Programari programare = findProgramare(id);
		System.out.println("Programarea cu  ID - ul  : " + programare.getIdProgramare() + " va avea loc in data de "
				+ programare.getData().getDate() + "." + (programare.getData().getMonth() + 1) + "."
				+ (programare.getData().getYear() + 1900) + " la ora " + programare.getOra() + ".");

		System.out.println("Aceasta va fi executata de Personalul Medical " + programare.getPersonalMedical().getNume()
				+ " " + programare.getPersonalMedical().getPrenume() + " cu ID - ul "
				+ programare.getPersonalMedical().getIdPersonalMedical() + ".");

		System.out.println("Programarea este adresata Animalului " + programare.getAnimal().getNume() + " cu ID - ul"
				+ " " + programare.getAnimal().getIdAnimal() + ".");
	}

	/**
	 * @param id Functia ofera posibilitatea modificarii campului Ora al Programarii
	 *           cu ID - ul egal cu valoarea parametrului primit id din tabelul
	 *           programari al Bazei de Date
	 */
	public void updateProgramare(int id) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduceti o noua ora (H si Min) : ");
		int hour = input.nextInt();
		int min = input.nextInt();

		Programari programare = findProgramare(id);
		if (programare == null) {
			System.out.println("Programarea nu a fost gasita");
		} else {
			@SuppressWarnings("deprecation")
			Time time = new Time(hour, min, 0);
			programare.setOra(time);
			entityManager.getTransaction().commit();
			System.out.println("Programarea a fost modificata cu succes! Verificati Baza de Date!");
		}
		input.close();
	}

	/**
	 * @param programare Functia ofera posibilitatea stergerii Programarii primite
	 *                   prin parametru programare din tabelul programari al Bazei
	 *                   de Date
	 */
	public void deleteProgramare(Programari programare) {
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}

	/**
	 * Functia afiseaza in Consola o lista a programarilor sortate dupa data, cu
	 * numele Animalului si a Personalului Medical.
	 */
	public void listaProgramariSortateDupaData() {
		System.out.println("Lista programarilor sortate dupa Data:");
		List<Programari> results = entityManager
				.createNativeQuery("Select * from veterinar.programari", Programari.class).getResultList();
		// results.sort(Comparator.comparing(Programari::getData).thenComparing(Programari::getOra));
		/**
		 * LAMBDA EXPRESSIONS
		 */
		results.sort((p1, p2) -> p1.getData().compareTo(p2.getData()));
		for (Programari programare : results) {
			System.out.println("Programarea cu ID - ul " + programare.getIdProgramare()
					+ " executata de Personalul Medical " + programare.getPersonalMedical().getNume() + " "
					+ programare.getPersonalMedical().getPrenume() + " pentru Animalul "
					+ programare.getAnimal().getNume() + ".");
		}
	}
}
