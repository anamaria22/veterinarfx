package controllers;

import java.io.IOException;
import java.sql.Time;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Programari;
import util.DatabaseUtil;

public class AppointmentController {

    @FXML
    private TextField textIdProgramare;
    @FXML
    private TextField textIdAnimal;

    @FXML
    private TextField textDataProgramare;

    @FXML
    private TextField textOraProgramare;

    @FXML
    private TextField textTipProgramare;

    @FXML
    private Button idNewAppButton;
    
    @FXML
    private Button buttonAnimal;
    
    @FXML
	private Label idAppointmentLabel;
    
    int idPersonal, idAnimal;
    @FXML
    public void getNewAppointmentInfo(ActionEvent e) {
       	
       	DatabaseUtil dbUtil = DatabaseUtil.getInstance();
   		try {
   			dbUtil.setUp();
   		} catch (Exception e1) {
   			// TODO Auto-generated catch block
   			e1.printStackTrace();
   		}
   		dbUtil.startTransaction();
   		
       	String newId = textIdProgramare.getText();
   		int newIdValue = Integer.parseInt(newId);
   		String newIdAnimal = textIdProgramare.getText();
   		int newIdAnimalValue = Integer.parseInt(newIdAnimal);
   		String newData=textDataProgramare.getText();
   		String[] tokens = newData.split("-");
   		int zi=Integer.parseInt(tokens[0]);
   		int luna=Integer.parseInt(tokens[1]);
   		int an=Integer.parseInt(tokens[2]);
   		Date data = new Date();
   		data.setDate(zi);
   		data.setMonth(luna - 1);
   		data.setYear(an - 1900);
   		String newHour=textOraProgramare.getText();
   		String[] tokensHour = newHour.split(":");
   		int ora=Integer.parseInt(tokensHour[0]);
   		int min=Integer.parseInt(tokensHour[1]);
   		Time time = new Time(ora, min, 0);
   		String newType=textTipProgramare.getText();
   		
   		Programari programare = new Programari();
   		programare.setIdProgramare(newIdValue);
   		programare.setData(data);
   		programare.setOra(time);
   		programare.setTip(newType);
   		
   		programare.setPersonalMedical(dbUtil.findPersonalMedical(idPersonal));
   		programare.setAnimal(dbUtil.findAnimal(newIdAnimalValue));
   		
   		dbUtil.createEntity(programare);
   		dbUtil.closeEntityManager();
   		
   		idAppointmentLabel.setText("Programarea a fost inregistrata cu succes!");
   		//try {
		//	Thread.sleep(1000);
		//} catch (InterruptedException e1) {
		//	// TODO Auto-generated catch block
		//	e1.printStackTrace();
		//}
 
   		}
   
    
    public void setInfo(int idUser,int idA) {
    idPersonal=idUser;
    idAnimal=idA;
    }
    
    @FXML
	public void showAnimals(ActionEvent event) throws IOException {

		Stage primaryStage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/controllers/Animals.fxml"));
		Parent root = (Parent) loader.load();
		AnimalsController secController = loader.getController();
		Scene scene = new Scene(root, 800, 800);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}