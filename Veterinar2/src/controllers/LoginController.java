package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LoginController implements Initializable {

	@FXML
	private Button idLogin;

	@FXML
	private TextField txtUserName;

	@FXML
	private PasswordField txtPassword;

	@FXML
	private TextField txtUserId;

	@FXML
	private Label idAccess;

	@FXML
	private Label idAppointmentLabel;
	public int id;
	public String name, idUser;

	ViewController c;

	@FXML
	void Login(ActionEvent event) throws Exception {

		idUser = txtUserId.getText();
		id = Integer.parseInt(idUser);
		name = txtUserName.getText();
		// createPersonalMedicalUser(id);
		if (txtPassword.getText().equals("pass")) {
			idAccess.setText("Login succeded!");

			Stage primaryStage = new Stage();

			FXMLLoader loader = new FXMLLoader(getClass().getResource("/controllers/View.fxml"));
			Parent root = (Parent) loader.load();
			ViewController secController = loader.getController();
			secController.setText(idUser);
			secController.populateMainListView();

			System.out.println(id + name);

			Scene scene = new Scene(root, 800, 800);
			primaryStage.setScene(scene);
			primaryStage.show();
		} else
			idAccess.setText("Login failed!");

	}

	@FXML
	public void setId() {
		idUser = txtUserId.getText();
		id = Integer.parseInt(idUser);
	}

	@FXML
	public void setName() {
		name = txtUserName.getText();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
}
