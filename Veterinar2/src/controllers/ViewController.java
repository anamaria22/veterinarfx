package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Animal;
import model.Diagnostic;
import model.Programari;
import util.DatabaseUtil;

public class ViewController implements Initializable {

	@FXML
	LoginController loginC;

	@FXML
	private DatePicker idData;

	@FXML
	private ListView<String> listV;

	@FXML
	private TextField idTipProgramare;

	@FXML
	private TableView<Diagnostic> idTable;

	@FXML
	private TableColumn<Diagnostic, String> idFirstColumn;

	@FXML
	private TableColumn<Diagnostic, String> idSecondColumn;

	@FXML
	private TableColumn<Diagnostic, String> idThirdColumn;

	@FXML
	private TextField txtNumeAnimal;

	@FXML
	private TextField txtVarstaAnimal;

	@FXML
	private TextField txtGreutateAnimal;

	@FXML
	private TextField txtSpecieAnimal;

	@FXML
	private TextField txtRasaAnimal;

	@FXML
	private TextField txtProprietarAnimal;

	@FXML
	private TextField txtTelefon;

	@FXML
	private ImageView idImageAnimal;

	@FXML
	private Label idLabel;

	@FXML
	private TextField textIdProgramare;

	@FXML
	private TextField textDataProgramare;

	@FXML
	private TextField textOraProgramare;

	@FXML
	private TextField textTipProgramare;

	@FXML
	private Button idButonProgramare;
	@FXML
	private Button idRefreshButton;
	
	@FXML
	private Button idRefreshDiagnostics;
	@FXML
	private Button idNewDiagnostic;
	
	int idCurrentAnimal;

	public ObservableList<Diagnostic> diagnosticList;
	public Set<Programari> programari;
	public List<Programari> programariDBList;

	@FXML
	void selectDate(ActionEvent event) {
		if (idData.getValue()==null)
			try {
				populateMainListView();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		else
		{
		String dataFromPicker = idData.getValue().toString();
		System.out.println(dataFromPicker);
		String[] tokens = dataFromPicker.split("-");
   		int dayPicker=Integer.parseInt(tokens[2]);
   		int monthPicker=Integer.parseInt(tokens[1]);
   		int yearPicker=Integer.parseInt(tokens[0]);
   		
   		DatabaseUtil db = DatabaseUtil.getInstance();
		try {
			db.setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.startTransaction();
		ObservableList<String> programariList = getProgramariObservableListAfterDate(dayPicker,monthPicker,yearPicker);
		listV.setItems(programariList);
		listV.refresh();
		db.closeEntityManager();
		}
	}
	public ObservableList<String> getProgramariObservableListAfterDate(int dayPicker,int monthPicker,int yearPicker) {
		
		ObservableList<String> prog = FXCollections.observableArrayList();
		// String idUser=txtUserId.getText();
		// int id=Integer.parseInt(idUser);
		for (Programari p : programariDBList) {
			String dataP=p.getStringData();
			String[] tokens = dataP.split("-");
			int day=Integer.parseInt(tokens[2]);
	   		int month=Integer.parseInt(tokens[1]);
	   		int year=Integer.parseInt(tokens[0]);
			if (day==dayPicker && month==monthPicker && year==yearPicker) {
				String programare = p.getAnimal().getIdAnimal() + " " + p.getOra().toString() + " " + p.getTip();
				prog.add(programare);
				System.out.println(p.getStringData());
			}
		}

		return prog;
	}

	@FXML
	public void populateMainListView() throws Exception {
		System.out.println(id);
		DatabaseUtil db = DatabaseUtil.getInstance();
		db.setUp();
		db.startTransaction();
		programariDBList = (List<Programari>) db.listaProgramari();
		System.out.println(db.listaProgramari().get(1).getData().toLocaleString());
		ObservableList<String> programariList = getProgramariObservableList(programariDBList);
		listV.setItems(programariList);
		listV.refresh();
		db.closeEntityManager();
	}


	@FXML
    void refreshListView(ActionEvent event) throws Exception {

    	listV.getItems().clear();
    	populateMainListView();
    }
	@FXML
	 void refreshListDiag(ActionEvent event) throws Exception {

	    	idTable.getItems().clear();
	    	populateDiagnosticTable(idCurrentAnimal);
	    }
	public int id;
	public String userName;
	LoginController l;

	public void getLoginInformation(int id, String userName) {
		this.id = id;
		this.userName = userName;
	}

	public void setText(String idUser) {
		idLabel.setText(idUser);
		setInfo();
	}

	public void setInfo() {
		String idUser = idLabel.getText();
		id = Integer.parseInt(idUser);
	}

	public ObservableList<Diagnostic> getDiagnosticsObservableList(List<Diagnostic> diagnostics, int idS) {
		ObservableList<Diagnostic> diag = FXCollections.observableArrayList();
		for (Diagnostic d : diagnostics) {
			if (d.getAnimal().getIdAnimal() == idS)
				diag.add(d);
		}
		return diag;
	}

	public ObservableList<String> getProgramariObservableList(List<Programari> programari) {
		programari.sort((p1, p2) -> p1.getData().compareTo(p2.getData()));
		ObservableList<String> prog = FXCollections.observableArrayList();
		// String idUser=txtUserId.getText();
		// int id=Integer.parseInt(idUser);
		for (Programari p : programari) {
			int idP = p.getPersonalMedical().getIdPersonalMedical();
			if (idP == id) {
				String programare = p.getAnimal().getIdAnimal() + " " + p.getOra().toString() + " " + p.getTip();
				prog.add(programare);
				System.out.println(p.getStringData());
			}
		}

		return prog;
	}

	public void populateDiagnosticTable(int idS) throws Exception {
		DatabaseUtil db = DatabaseUtil.getInstance();
		db.setUp();
		db.startTransaction();
		List<Diagnostic> diagnosticDBList = (List<Diagnostic>) db.diagnosticList();
		diagnosticList = getDiagnosticsObservableList(diagnosticDBList, idS);
		idTable.setItems(diagnosticList);
		db.closeEntityManager();

	}

	@FXML
	public void showInfo(ActionEvent event) {
//			listV.getSelectionModel().selectedItemProperty()
//			.addListener(new ChangeListener<String>() {
//
//				public void changed(
//						ObservableValue<? extends String> observable,
//						String oldValue, String newValue) {
//					// change the label text value to the newly selected
//					// item.
//					System.out.println("whatever");
//					txtNumeAnimal.setText("You Selected " + newValue);
//				}
//			});
	}

	@FXML
	public void addProgrammaris(ActionEvent event) throws IOException {

		Stage primaryStage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/controllers/Appointment.fxml"));
		Parent root = (Parent) loader.load();
		AppointmentController secController = loader.getController();
		secController.setInfo(id, idCurrentAnimal);
		Scene scene = new Scene(root, 800, 800);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	 @FXML
		public void addDiagnostic(ActionEvent event) throws IOException {

			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/controllers/Diagnostic.fxml"));
			Parent root = (Parent) loader.load();
			DiagnosticController secController = loader.getController();
			secController.setInfo(id, idCurrentAnimal);
			Scene scene = new Scene(root, 800, 800);
			primaryStage.setScene(scene);
			primaryStage.show();
		}

	public void initialize(URL location, ResourceBundle resources) {
		// setInfo();
		// getLoginInformation(id,userName);
		/*
		 * try { populateMainListView();
		 * 
		 * } catch(Exception e) { e.printStackTrace();
		 * 
		 * } //System.out.println(id+"////"+userName);
		 * 
		 * //System.out.println(id+"////"+userName); try { populateDiagnosticTable();
		 * idFirstColumn.setCellValueFactory(new
		 * PropertyValueFactory<Diagnostic,String>("data"));
		 * idSecondColumn.setCellValueFactory(new
		 * PropertyValueFactory<Diagnostic,String>("titlu"));
		 * idThirdColumn.setCellValueFactory(new
		 * PropertyValueFactory<Diagnostic,String>("descriere")); } catch (Exception e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

		// idImageAnimal.setImage(new Image("C:\\Users\\Ana\\Desktop\\doggy.jpg"));

		listV.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				// change the label text value to the newly selected
				// item.
				System.out.println("whatever");
				String[] idSeelecteAnimal = newValue.split(" ");
				idCurrentAnimal = Integer.parseInt(idSeelecteAnimal[0].toString());
				DatabaseUtil db = DatabaseUtil.getInstance();
				try {
					db.setUp();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				db.startTransaction();
				Animal selectedAnimal = db.findAnimal(idCurrentAnimal);

				System.out.println(idCurrentAnimal);
				idTipProgramare.setText(idSeelecteAnimal[2]);
				txtNumeAnimal.setText("Nume: " + selectedAnimal.getNume());
				txtVarstaAnimal.setText("Varsta: " + selectedAnimal.getVarsta());
				String greutate = String.valueOf(selectedAnimal.getGreutate());
				txtGreutateAnimal.setText("Greutate: " + greutate);
				txtSpecieAnimal.setText("Specie: " + selectedAnimal.getSpecie());
				txtRasaAnimal.setText("Rasa: " + selectedAnimal.getRasa());
				txtProprietarAnimal.setText("Proprietar: " + selectedAnimal.getProprietar());
				txtTelefon.setText("Telefon: " + selectedAnimal.getTelefon());
				try {
					populateDiagnosticTable(idCurrentAnimal);
					idFirstColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic, String>("data"));
					idSecondColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic, String>("titlu"));
					idThirdColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic, String>("descriere"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
