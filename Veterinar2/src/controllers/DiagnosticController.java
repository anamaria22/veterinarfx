package controllers;

import java.sql.Time;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Diagnostic;
import model.Programari;
import util.DatabaseUtil;

public class DiagnosticController {

    @FXML
    private TextField textIdDiagnostic;

    @FXML
    private TextField textIdData;

    @FXML
    private TextField textIdTitlu;

    @FXML
    private TextField textIdDescriere;

    @FXML
    private Button idNewDiagnostic;

    @FXML
    private Label idDiagnosticLabel;

    int idPersonal, idAnimal;
    @FXML
    public void getNewDiagnosticInfo(ActionEvent e) {
       	
       	DatabaseUtil dbUtil = DatabaseUtil.getInstance();
   		try {
   			dbUtil.setUp();
   		} catch (Exception e1) {
   			// TODO Auto-generated catch block
   			e1.printStackTrace();
   		}
   		dbUtil.startTransaction();
   		
       	String newId = textIdDiagnostic.getText();
   		int newIdValue = Integer.parseInt(newId);
   		String newData=textIdData.getText();
   		String[] tokens = newData.split("-");
   		int zi=Integer.parseInt(tokens[0]);
   		int luna=Integer.parseInt(tokens[1]);
   		int an=Integer.parseInt(tokens[2]);
   		Date data = new Date();
   		data.setDate(zi);
   		data.setMonth(luna - 1);
   		data.setYear(an - 1900);
   		
   		String newTitle=textIdTitlu.getText();
   		String newDescription=textIdDescriere.getText();
   		Diagnostic diagnostic=new Diagnostic();
   		diagnostic.setIdDiagnostic(newIdValue);
   		diagnostic.setData(data);
   		diagnostic.setTitlu(newTitle);
   		diagnostic.setDescriere(newDescription);
   		diagnostic.setPersonalMedical(dbUtil.findPersonalMedical(idPersonal));
   		diagnostic.setAnimal(dbUtil.findAnimal(idAnimal));
   		dbUtil.createEntity(diagnostic);
   		dbUtil.closeEntityManager();
   		
   		idDiagnosticLabel.setText("Diagnosticul a fost inregistrat cu succes!");
   		//try {
		//	Thread.sleep(1000);
		//} catch (InterruptedException e1) {
		//	// TODO Auto-generated catch block
		//	e1.printStackTrace();
		//}
 
   		}
    
    public void setInfo(int idUser,int idA) {
    idPersonal=idUser;
    idAnimal=idA;
    }
}