package controllers;


import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Animal;
import model.Diagnostic;
import util.DatabaseUtil;

public class AnimalsController implements Initializable {

    @FXML
    private TableView<Animal> idTable;

    @FXML
    private TableColumn<Animal, String> idCol;

    @FXML
    private TableColumn<Animal, String> nameCol;

    @FXML
    private TableColumn<Animal, String> ageCol;

    @FXML
    private TableColumn<Animal, Float> weightCol;

    @FXML
    private TableColumn<Animal,  String> speciesCol;

    @FXML
    private TableColumn<Animal,  String> breedCol;

    @FXML
    private TableColumn<Animal,  String> ownerCol;

    @FXML
    private TableColumn<Animal,  String> phoneCol;

    @FXML
    private TableColumn<Animal,  String> emailCol;
    public ObservableList<Animal> animalList;

	public void populateAnimalsTable() throws Exception {
		DatabaseUtil db = DatabaseUtil.getInstance();
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		animalList = getDiagnosticsObservableList(animalDBList);
		idTable.setItems(animalList);
		db.closeEntityManager();

	}
	public ObservableList<Animal> getDiagnosticsObservableList(List<Animal> animals) {
		ObservableList<Animal> an= FXCollections.observableArrayList();
			for (Animal a : animals) {
				an.add(a);
		}
		return an;
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			
			idCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("idAnimal"));
			nameCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("nume"));
			ageCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("varsta"));
			weightCol.setCellValueFactory(new PropertyValueFactory<Animal, Float>("greutate"));
			speciesCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("specie"));
			breedCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("rasa"));
			ownerCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("proprietar"));
			phoneCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("telefon"));
			emailCol.setCellValueFactory(new PropertyValueFactory<Animal, String>("email"));
			populateAnimalsTable();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
