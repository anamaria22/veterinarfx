
package main;

import java.sql.Time;
import java.util.Date;
import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.PersonalMedical;
import model.Programari;
import util.DatabaseUtil;

/**
 * @author Ana
 * Aceasta aplicatie comunica cu o baza de date din Workbench mysql, in vederea
 * planificarii consultatiilor la un cabinet veterinar.
 */

/**
 * Aceasta este clasa principala Main, ce contine un meniu principal, ce
 * faciliteaza accesul la operatiile CRUD pentru cele 3 clase ( Animal,
 * PersonalMedical, Programari) si totodata ofera o interfata prietenoasa pentru
 * utilizator, prin intermediul consolei.
 */
public class Main extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			Parent root= FXMLLoader.load(getClass().getResource("/controllers/Login.fxml"));
			//BorderPane root=(BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene =new Scene(root,400,400);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}
		catch (Exception e){
		e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}
}

/**
 * @throws Exception - functia Main poate arunca diverse Exceptii generate de :
 *                   - functia setUp() in cazul in care nu se poate stabili
 *                   conexiunea la baza de date - functiile createAnimal(),
 *                   createPersonalMedical() sau createProgramare() in cazul in
 *                   care un animal, un personal medial, sau o programare nu
 *                   poate fi creata din motive precum inserarea unui ID deja
 *                   existent, lucru imposibil, intrucat acesta este UNIC, fiind
 *                   setat drept CHEIE PRIMARA
 */

/*
 public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();

		System.out.println("---------------------------------------------------------------");
		System.out.println("|    Alegeti cifra corespunzatoare optiunuii dumnevoastra:    |");
		System.out.println("|                                                             |");
		System.out.println("|                     1. CRUD Animal                          |");
		System.out.println("|                     2. CRUD Personal Medical                |");
		System.out.println("|                     3. CRUD Programari                      |");
		System.out.println("|                     4. AFISARE Lista Programari             |");
		System.out.println("---------------------------------------------------------------");

		int mainOption = 0;
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		mainOption = input.nextInt();

		switch (mainOption) {
		case 1: {

			System.out.println("---------------------------------------------------------------");
			System.out.println("|    Alegeti litera corespunzatoare optiunuii dumnevoastra:    |");
			System.out.println("|                                                              |");
			System.out.println("|                      C. Create Animal                        |");
			System.out.println("|                      R. Read Animal                          |");
			System.out.println("|                      U. Update Animal                        |");
			System.out.println("|                      D. Delete Animal                        |");
			System.out.println("----------------------------------------------------------------");

			// Scanner input=new Scanner(System.in);
			char option = input.next().charAt(0);

			switch (option) {
			case 'C': {
				Scanner util = new Scanner(System.in);
				
				System.out.println("Introduceti ID - ul Animalului: ");
				int id=util.nextInt();
				System.out.println("ID - ul  este: "+id);
				
				System.out.println("Introduceti Numele Animalului: ");
				String nume = util.next();
				System.out.println("Numele este: "+nume);
				// String nume = System.console().readLine();
				//System.out.println();
				
				System.out.println("Introduceti Varsta Animalului: ");
				 //String varsta = System.console().readLine();
				String varsta = util.next();
				//util.close();
				System.out.println("Varsta este: "+varsta);
				
				//System.out.println();
				
				
				System.out.println("Introduceti Greutatea Animalului: ");
				float greutate=util.nextFloat();
				System.out.println("Greutatea este: "+greutate);
				
				
				System.out.println("Introduceti Specia Animalului: ");
				String specie=util.next();
				System.out.println("Specia este: "+specie);
				
				// String specie = System.console().readLine();
			   // System.out.println();
				
				System.out.println("Introduceti Rasa Animalului: ");
				String rasa=util.next();
				System.out.println("Rasa este: "+rasa);
				
				 //String rasa = System.console().readLine();
				//System.out.println();
				
				System.out.println("Introduceti Proprietarul Animalului: ");
				String proprietar=util.next();
				System.out.println("Numele proprietarului este: "+proprietar);
				
				// String proprietar = System.console().readLine();
				//System.out.println();
				
				System.out.println("Introduceti Telefonul proprietarului Animalului: ");
				String telefon=util.next();
				System.out.println("Telefonul este: "+telefon);
				
				// String telefon = System.console().readLine();
				//System.out.println();
				
				System.out.println("Introduceti Email-ul Animalului: ");
				String email=util.next();
				System.out.println("Emailul este: "+email);
				
				 //String email = System.console().readLine();
				System.out.println();
				
				
				
				Animal animal = new Animal();
				animal.constructAnimal(id,nume, varsta, greutate, specie, rasa, proprietar, telefon, email);
				/*animal.setNume(nume);
				animal.setVarsta(varsta);
				animal.setGreutate(greutate);
				animal.setSpecie(specie);
				animal.setRasa(rasa);
				animal.setProprietar(proprietar);
				animal.setTelefon(telefon);
				animal.setEmail(email);
				dbUtil.createAnimal(animal);
				util.close();
				return;
			}

			case 'R': {
				System.out.println(
						"Introduceti ID-ul Animalului despre care doriti sa afisati informatiile disponibile:");
				int id = input.nextInt();
				if (dbUtil.findAnimal(id) == null)
					System.out.println("Nu exista un Animal cu acest ID!");
				else
					dbUtil.readAnimal(id);
				return;
			}
			case 'U': {
				System.out.println("Introduceti ID-ul Animalului pe care doriti sa il modificati:");
				int id = input.nextInt();
				if (dbUtil.findAnimal(id) == null)
					System.out.println("Nu exista un Animal cu acest ID!");
				else
					dbUtil.updateAnimal(id);
				return;
			}
			case 'D': {
				System.out.println("Introduceti ID-ul Animalului pe care doriti sa il stergeti:");
				int id = input.nextInt();
				if (dbUtil.findAnimal(id) == null)
					System.out.println("Nu exista un Animal cu acest ID!");
				else
					dbUtil.deleteAnimal(dbUtil.findAnimal(id));
				return;
			}

			}
		}

		case 2: {
			System.out.println("----------------------------------------------------------------");
			System.out.println("|    Alegeti litera corespunzatoare optiunuii dumnevoastra:    |");
			System.out.println("|                                                              |");
			System.out.println("|                 C. Create Personal Medical                   |");
			System.out.println("|                 R. Read Personal Medical                     |");
			System.out.println("|                 U. Update Personal Medical                   |");
			System.out.println("|                 D. Delete Personal Medical                   |");
			System.out.println("----------------------------------------------------------------");

			char option = input.next().charAt(0);
			switch (option) {
			case 'C': {
				Scanner util = new Scanner(System.in);
				System.out.println("Introduceti numele Personalului Medical: ");
				String nume = util.nextLine();
				System.out.println("Introduceti prenumele Personalului Medical: ");
				String prenume = util.nextLine();
				System.out.println("Introduceti ID - ul Personalului Medical: ");
				int id = input.nextInt();

				PersonalMedical personalMedical = new PersonalMedical();
				personalMedical.setIdPersonalMedical(id);
				personalMedical.setNume(nume);
				personalMedical.setPrenume(prenume);
				dbUtil.createPersonalMedical(personalMedical);
				util.close();
				return;
			}
			case 'R': {
				System.out.println(
						"Introduceti ID-ul Personalului Medical despre care doriti sa afisati informatiile disponibile:");
				int id = input.nextInt();
				if (dbUtil.findPersonalMedical(id) == null)
					System.out.println("Nu exista un Personal Medical cu acest ID!");
				else
					dbUtil.readPersonalMedical(id);
				return;
			}
			case 'U': {
				System.out.println("Introduceti ID-ul Personalului Medical pe care doriti sa il modificati:");
				int id = input.nextInt();
				if (dbUtil.findPersonalMedical(id) == null)
					System.out.println("Nu exista un Personal Medical cu acest ID!");
				else
					dbUtil.updatePersonalMedical(id);
				return;
			}
			case 'D': {
				System.out.println("Introduceti ID-ul Personalului Medical pe care doriti sa il stergeti:");
				int id = input.nextInt();
				if (dbUtil.findPersonalMedical(id) == null)
					System.out.println("Nu exista un PersonalMedical cu acest ID!");
				else
					dbUtil.deletePersonalMedical(dbUtil.findPersonalMedical(id));
				return;
			}
			}

		}

		case 3: {
			System.out.println("---------------------------------------------------------------");
			System.out.println("|    Alegeti litera corespunzatoare optiunuii dumnevoastra:    |");
			System.out.println("|                                                              |");
			System.out.println("|                    C. Create Programare                      |");
			System.out.println("|                    R. Read Programare                        |");
			System.out.println("|                    U. Update Programare                      |");
			System.out.println("|                    D. Delete Programare                      |");
			System.out.println("----------------------------------------------------------------");

			char option = input.next().charAt(0);
			switch (option) {
			case 'C': {
				Scanner util = new Scanner(System.in);

				System.out.println("Introduceti ID - ul Programarii: ");
				int id = util.nextInt();

				System.out.println("Introduceti DATA programarii ( ZI - LUNA - AN ) ");

				int zi = util.nextInt();
				int luna = util.nextInt();
				int an = util.nextInt();

				Date data = new Date();
				data.setDate(zi);
				data.setMonth(luna - 1);
				data.setYear(an - 1900);

				System.out.println("Introduceti ORA programarii ( ORA - MIN ) ");
				int ora = util.nextInt();
				int min = util.nextInt();
				Time time = new Time(ora, min, 0);

				System.out.println();
				dbUtil.printAllAnimalsFromDB();
				System.out.println("Introduceti ID - ul Animalului: ");
				int idAnimal = input.nextInt();

				System.out.println();
				dbUtil.printAllPersonalMedicalFromDB();
				System.out.println("Introduceti ID - ul Personalului Medical: ");
				int idPersonal = input.nextInt();

				Programari programare = new Programari();
				programare.setIdProgramare(id);
				programare.setData(data);
				programare.setOra(time);

				programare.setAnimal(dbUtil.findAnimal(idAnimal));
				dbUtil.findAnimal(idAnimal).addProgramari(programare);

				programare.setPersonalMedical(dbUtil.findPersonalMedical(idPersonal));
				dbUtil.findPersonalMedical(idPersonal).addProgramari(programare);
				dbUtil.createProgramare(programare);

				return;
			}
			case 'R': {
				System.out.println(
						"Introduceti ID-ul Programarii despre care doriti sa afisati informatiile disponibile:");
				int id = input.nextInt();
				if (dbUtil.findProgramare(id) == null)
					System.out.println("Nu exista o Programare cu acest ID!");
				else
					dbUtil.readProgramare(id);
				return;
			}
			case 'U': {
				System.out.println("Introduceti ID-ul Programarii pe care doriti sa o modificati:");
				int id = input.nextInt();
				if (dbUtil.findProgramare(id) == null)
					System.out.println("Nu exista o Programare cu acest ID!");
				else
					dbUtil.updateProgramare(id);
				return;
			}
			case 'D': {
				System.out.println("Introduceti ID-ul Programarii pe care doriti sa o stergeti:");
				int id = input.nextInt();
				if (dbUtil.findProgramare(id) == null)
					System.out.println("Nu exista o Programare cu acest ID!");
				else
					dbUtil.deleteProgramare(dbUtil.findProgramare(id));
				return;
			}
			}

		}
		case 4: {
			dbUtil.listaProgramariSortateDupaData();
		}
		
	}
}

 */
