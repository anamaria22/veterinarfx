package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	@Temporal(TemporalType.DATE)
	private Date data;

	private Time ora;

	private String tip;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to PersonalMedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	private PersonalMedical personalMedical;

	public Programari() {
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public Date getData() {
		return this.data;
	}
public String getStringData()
{
	Date data=getData();
	int year=data.getYear()+1900;
	int month=data.getMonth()+1;
	int day=data.getDate();
	String stringDate=""+year+'-'+month+'-'+day;
	return stringDate;
}
	public void setData(Date data) {
		this.data = data;
	}

	public Time getOra() {
		return this.ora;
	}

	public void setOra(Time ora) {
		this.ora = ora;
	}

	public String getTip() {
		return this.tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public PersonalMedical getPersonalMedical() {
		return this.personalMedical;
	}

	public void setPersonalMedical(PersonalMedical personalMedical) {
		this.personalMedical = personalMedical;
	}

}