package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personal_medical database table.
 * 
 */
@Entity
@Table(name="personal_medical")
@NamedQuery(name="PersonalMedical.findAll", query="SELECT p FROM PersonalMedical p")
public class PersonalMedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonalMedical;

	private String email;

	private String nume;

	private String prenume;

	private String specializare;

	private String telefon;

	private int varsta;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="personalMedical")
	private List<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="personalMedical")
	private List<Programari> programaris;

	public PersonalMedical() {
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return this.prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getSpecializare() {
		return this.specializare;
	}

	public void setSpecializare(String specializare) {
		this.specializare = specializare;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getVarsta() {
		return this.varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	public List<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	public void setDiagnostics(List<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setPersonalMedical(this);

		return diagnostic;
	}

	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setPersonalMedical(null);

		return diagnostic;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setPersonalMedical(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setPersonalMedical(null);

		return programari;
	}

}