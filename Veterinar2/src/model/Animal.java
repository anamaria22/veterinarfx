package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	private String email;

	private float greutate;

	private String nume;

	private String proprietar;

	private String rasa;

	private String specie;

	private String telefon;

	private String varsta;

	//bi-directional many-to-one association to Diagnostic
	@OneToMany(mappedBy="animal")
	private List<Diagnostic> diagnostics;

	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="animal")
	private List<Programari> programaris;

	public Animal() {
	}
	public void constructAnimal(int id,String nume, String varsta, float greutate,String specie, String rasa, String proprietar,String telefon,String email) {
		this.setIdAnimal(id);
		this.setNume(nume);
		this.setVarsta(varsta);
		this.setGreutate(greutate);
		this.setSpecie(specie);
		this.setRasa(rasa);
		this.setProprietar(proprietar);
		this.setTelefon(telefon);
		this.setEmail(email);
	}
	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getGreutate() {
		return this.greutate;
	}

	public void setGreutate(float greutate) {
		this.greutate = greutate;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getProprietar() {
		return this.proprietar;
	}

	public void setProprietar(String proprietar) {
		this.proprietar = proprietar;
	}

	public String getRasa() {
		return this.rasa;
	}

	public void setRasa(String rasa) {
		this.rasa = rasa;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getVarsta() {
		return this.varsta;
	}

	public void setVarsta(String varsta) {
		this.varsta = varsta;
	}

	public List<Diagnostic> getDiagnostics() {
		return this.diagnostics;
	}

	public void setDiagnostics(List<Diagnostic> diagnostics) {
		this.diagnostics = diagnostics;
	}

	public Diagnostic addDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().add(diagnostic);
		diagnostic.setAnimal(this);

		return diagnostic;
	}

	public Diagnostic removeDiagnostic(Diagnostic diagnostic) {
		getDiagnostics().remove(diagnostic);
		diagnostic.setAnimal(null);

		return diagnostic;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setAnimal(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setAnimal(null);

		return programari;
	}

}