package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.Table;

import com.sun.tools.xjc.model.SymbolSpace;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Animal;
import model.Diagnostic;
import model.PersonalMedical;
import model.Programari;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	 private ListView<String> listView;
	@FXML
	 private ListView<String> listV;
	@FXML
	private Label idAccess;
	
	@FXML
	private TextField txtUserName;
	@FXML
	private TextField txtPassword;
	@FXML
	private TextField txtUserId;
	
	@FXML
	private ImageView idImageAnimal;
	@FXML
	private TextField txtNumeAnimal;
	@FXML
	private TextField txtVarstaAnimal;
	@FXML
	private TextField txtGreutateAnimal;
	@FXML
	private TextField txtSpecieAnimal;
	@FXML
	private TextField txtRasaAnimal;
	@FXML
	private TextField txtProprietarAnimal;
	@FXML
	private TextField idTipProgramare;
	@FXML
	private DatePicker idData; 
	
	@FXML
	private TableView<Diagnostic> idTable;
	
	@FXML 
	private TableColumn<Diagnostic,String> idFirstColumn;
	 
	@FXML 
	private TableColumn<Diagnostic,String> idSecondColumn;
	    
	@FXML 
	private TableColumn<Diagnostic,String> idThirdColumn ;
	
	public String idUser;
	public int id;
	public PersonalMedical personalMedicalUser;
	
	public void Login(ActionEvent event) throws Exception  {
		
		 idUser=txtUserId.getText();
		 id=Integer.parseInt(idUser);
	
		if (txtPassword.getText().equals("pass")) {
			idAccess.setText("Login succeded!");
			Stage primaryStage=new Stage();
			BorderPane root=(BorderPane) FXMLLoader.load(getClass().getResource("C:\\Users\\Ana\\Desktop\\veterinarWorkspace\\VeterinarFX\\src\\controllers\\View.fxml"));
			Scene scene =new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();		
		}
		else 
			idAccess.setText("Login failed!");	
	}
	
	public void createPersonalMedicalUser(int id) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		personalMedicalUser=db.findPersonalMedical(id);
		System.out.println(personalMedicalUser.getNume());
		db.closeEntityManager();
	}
	
	public void populateDiagnosticTable() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Diagnostic> diagnosticDBList = (List<Diagnostic>)db.diagnosticList();
		ObservableList<Diagnostic> diagnosticList = getDiagnosticsObservableList(diagnosticDBList);
		idTable.setItems(diagnosticList);
		db.closeEntityManager();
		
	}
	public void populateMainListView() throws Exception  {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Programari> programariDBList = (List<Programari>)db.listaProgramari();
		ObservableList<String> programariList = getProgramariObservableList(programariDBList);
		listV.setItems(programariList);
		listV.refresh();
		db.closeEntityManager();
	}
		
	public ObservableList<Diagnostic> getDiagnosticsObservableList(List<Diagnostic> diagnostics) {
		ObservableList<Diagnostic> diag = FXCollections.observableArrayList();
		for (Diagnostic d: diagnostics) {
				diag.add(d);
				}
		return diag;
	}
	
	public ObservableList<String> getProgramariObservableList(List<Programari> programari) 
	{
		ObservableList<String> prog = FXCollections.observableArrayList();
		//String idUser=txtUserId.getText();
		//int id=Integer.parseInt(idUser);
		for (Programari p: programari) 
		{
			//int idP=p.getPersonalMedical().getIdPersonalMedical();
			//if(idP==id) 
			{
					String programare=p.getOra().toString()+" "+p.getTip();
					prog.add(programare);
			}
		}
		return prog;
	}
	
	public ObservableList<Time> getOreProgramariObservableList(List<Programari> programari) {
		ObservableList<Time> ore = FXCollections.observableArrayList();
		for (Programari p: programari) {
			ore.add(p.getOra());
		}
		return ore;
	}
	
	public ObservableList<String> getTipProgramariObservableList(List<Programari> programari) {
		ObservableList<String> tipuri = FXCollections.observableArrayList();
		for (Programari p: programari) {
			tipuri.add(p.getTip());
		}
		return tipuri;
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			populateDiagnosticTable();
			idFirstColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic,String>("data"));
		    idSecondColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic,String>("titlu"));
			idThirdColumn.setCellValueFactory(new PropertyValueFactory<Diagnostic,String>("descriere"));	
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}
}	
